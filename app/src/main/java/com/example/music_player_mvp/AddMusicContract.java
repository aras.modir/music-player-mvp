package com.example.music_player_mvp;

public interface AddMusicContract {

    interface View {
        void afterSave();

        void showListPage();


    }

    interface Presenter {
        void attachView(View view);

        void addMusic(MusicPOJO musicPOJO);

        void afterSave();

        void list();


    }

    interface Model {
        void attatchPresenter(Presenter presenter);

        void addMusic(MusicPOJO musicPOJO);

    }
}
