package com.example.music_player_mvp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.music_player_mvp.ListMusic.MusicListsActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity implements AddMusicContract.View {
    EditText title, cover, url, singer, album;
    Button save, list;
    AddMusicContract.Presenter presenter = new AddMusicPresenter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title = findViewById(R.id.title);
        cover = findViewById(R.id.cover);
        url = findViewById(R.id.url);
        singer = findViewById(R.id.singer);
        album = findViewById(R.id.album);
        save = findViewById(R.id.save);
        list = findViewById(R.id.list);

        presenter.attachView(this);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MusicPOJO musicPOJO = MusicPOJO.newBuilder()
                        .album(album.getText().toString())
                        .cover(cover.getText().toString())
                        .singer(singer.getText().toString())
                        .url(url.getText().toString())
                        .title(title.getText().toString())
                        .build();
                presenter.addMusic(musicPOJO);
            }
        });

        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.list();
            }
        });

    }

    @Override
    public void afterSave() {
        clean();
        Toast.makeText(this, "Thank you, New Music Has Been Added", Toast.LENGTH_SHORT).show();
        Toast.makeText(this, getMusics().size() + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showListPage() {
        Intent intent = new Intent(MainActivity.this, MusicListsActivity.class);
        startActivity(intent);
    }

    void clean() {
        title.setText("");
        album.setText("");
        cover.setText("");
        singer.setText("");
        url.setText("");
    }

    public static List<MusicPOJO> getMusics() {
        return MusicPOJO.listAll(MusicPOJO.class);
    }
}
