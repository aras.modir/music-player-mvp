package com.example.music_player_mvp;

public class AddMusicModel implements AddMusicContract.Model {
    private AddMusicContract.Presenter presenter;

    @Override
    public void attatchPresenter(AddMusicContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void addMusic(MusicPOJO musicPOJO) {
        musicPOJO.save();
        presenter.afterSave();
    }
}
