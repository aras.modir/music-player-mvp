package com.example.music_player_mvp.ListMusic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.music_player_mvp.DetailsMusicActivity;
import com.example.music_player_mvp.MusicPOJO;
import com.example.music_player_mvp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MusicListAdapter extends BaseAdapter {
    Context mContext;
    List<MusicPOJO> musics;
    private ImageView cover;

    public MusicListAdapter(Context mContext, List<MusicPOJO> musics) {
        this.mContext = mContext;
        this.musics = musics;
    }

    @Override
    public int getCount() {
        return musics.size();
    }

    @Override
    public Object getItem(int position) {
        return musics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.music_list_item, parent, false);
        TextView title = view.findViewById(R.id.title);
        title.setText(musics.get(position).getTitle());
        cover = view.findViewById(R.id.cover);
        Picasso.get().load(musics.get(position).getCover()).into(cover);
        return view;
    }

    public void handleClick(MusicPOJO musicPOJO) {
        Intent detail = new Intent(mContext, DetailsMusicActivity.class);
        detail.putExtra("cover", musicPOJO.getCover());
        Activity mActivity = (Activity) mContext;
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity
        , cover, "img");
        mContext.startActivity(detail, options.toBundle());

    }

}
