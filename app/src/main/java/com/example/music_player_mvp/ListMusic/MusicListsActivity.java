package com.example.music_player_mvp.ListMusic;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.music_player_mvp.AddMusicContract;
import com.example.music_player_mvp.DetailsMusicActivity;
import com.example.music_player_mvp.MusicPOJO;
import com.example.music_player_mvp.MusicPlayerService;
import com.example.music_player_mvp.R;

import java.util.List;

public class MusicListsActivity extends AppCompatActivity implements ListContract.View {
    ListContract.Presenter presenter = new ListPresenter();
    ListView listView;
    MusicListAdapter adapter;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_lists);
        listView = findViewById(R.id.list);

        presenter.attachView(this);
    }

    @Override
    public void onLoadedMusics(List<MusicPOJO> musics) {
        adapter = new MusicListAdapter(MusicListsActivity.this, musics);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MusicPOJO musicPOJO = (MusicPOJO) parent.getItemAtPosition(position);
                presenter.musicSelected(musicPOJO);
            }
        });
    }

    @Override
    public void playMusic(MusicPOJO musics) {
        Intent playService = new Intent(this, MusicPlayerService.class);
        playService.putExtra("music_id", musics.getId() + "");
        startService(playService);

        adapter.handleClick(musics);

    }
}
