package com.example.music_player_mvp.ListMusic;


import com.example.music_player_mvp.MusicPOJO;

import java.util.List;

public interface ListContract {

    interface View {
        void onLoadedMusics(List<MusicPOJO> musics);
        void playMusic(MusicPOJO musics);
    }

    interface Presenter {
        void attachView(View view);

        void onLoadedMusics(List<MusicPOJO> musics);

        void musicSelected(MusicPOJO musics);

    }

    interface Model {
        void attatchPresenter(Presenter presenter);

        void loadMusics();

    }
}
