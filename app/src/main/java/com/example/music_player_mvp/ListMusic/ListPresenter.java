package com.example.music_player_mvp.ListMusic;

import com.example.music_player_mvp.MusicPOJO;

import java.util.List;

public class ListPresenter implements ListContract.Presenter {
    private ListContract.View view;
    ListContract.Model model = new ListModel();

    @Override
    public void attachView(ListContract.View view) {

        this.view = view;
        model.attatchPresenter(this);
        model.loadMusics();
    }

    @Override
    public void onLoadedMusics(List<MusicPOJO> musics) {
        view.onLoadedMusics(musics);
    }

    @Override
    public void musicSelected(MusicPOJO musics) {
        view.playMusic(musics);
    }
}
