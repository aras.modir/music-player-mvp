package com.example.music_player_mvp.ListMusic;

import com.example.music_player_mvp.MusicPOJO;

import java.util.List;

public class ListModel implements ListContract.Model {
    private ListContract.Presenter presenter;

    @Override
    public void attatchPresenter(ListContract.Presenter presenter) {

        this.presenter = presenter;
    }

    @Override
    public void loadMusics() {
        presenter.onLoadedMusics(getMusics());
    }

    public List<MusicPOJO> getMusics(){
        return MusicPOJO.listAll(MusicPOJO.class);
    }
}
