package com.example.music_player_mvp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class DetailsMusicActivity extends AppCompatActivity {
    ImageView cover;
    TextView title, album;
    ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_music);

        cover = findViewById(R.id.cover);
        title = findViewById(R.id.title);
        album = findViewById(R.id.album);
        progress = findViewById(R.id.progress);

        if (getIntent().hasExtra("cover")) {
            Picasso.get().load(getIntent().getStringExtra("cover"));
        }
    }

    @Subscribe
    public void onMusicPlaying(MusicPOJO musicPOJO) {
        album.setText(musicPOJO.getAlbum());
        progress.setProgress(musicPOJO.getPercent());
        title.setText(musicPOJO.getTitle());
        Picasso.get().load(musicPOJO.getCover()).into(cover);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
